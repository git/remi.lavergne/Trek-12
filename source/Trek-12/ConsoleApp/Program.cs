// See https://aka.ms/new-console-template for more information

using Models;
using Models.Events;
using Models.Game;
using Models.Interfaces;
using DataContractPersistence;
using Microsoft.Extensions.Logging;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;

namespace ConsoleApp;

class Program
{

    static Game Game { get; set; }

    /// <summary>
    /// Main function of the console app
    /// </summary>
    /// <param name="args"></param>
    static void Main(string[] args)
    {
        Console.WriteLine("Enter your pseudo:");
        string? pseudo = Console.ReadLine();
        if (pseudo != null)
        {
            IPersistence persistence = new DataContractXml();
            Player player = new Player(pseudo, "test.png");

            Map map = new Map("Dunai","background");
            Game = new Game(persistence);

            // Abonnement aux événements
            Game.GameStarted += OnGameStarted!;
            Game.GameEnded += OnGameEnded!;
            Game.BoardUpdated += OnBoardUpdated!;
            Game.DiceRolled += OnDiceRolled!;
            Game.OperationChosen += OnOperationChosen!;
            Game.CellChosen += OnCellChosen!;
            Game.PlayerChooseOp += OnPlayerSelectionOp!;
            Game.PlayerOption += OnPlayerOption!;
            Game.PlayerChooseCell += OnPlayerSelectionCell!;

            // Initialisation
            Game.InitializeGame(map, player);
            Game.GameLoop();
        }


    }

    static void OnPlayerSelectionCell(Object sender, PlayerChooseCellEventArgs e)
    {
        int row, column;
        while (true)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Enter the position of the cell you want to play");
            Console.WriteLine("Enter the row number (0-6)");
            Console.ResetColor();
            if (!int.TryParse(Console.ReadLine(), out row) || row < 0 || row >= 7)
            {
                Console.ForegroundColor= ConsoleColor.Red;
                Console.WriteLine("Invalid row number. Please enter a number between 0 and 6.");
                Console.ResetColor();
                continue;
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Enter the column number (0-6)");
            Console.ResetColor();
            if (!int.TryParse(Console.ReadLine(), out column) || column < 0 || column >= 7)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid column number. Please enter a number between 0 and 6.");
                Console.ResetColor();
                continue;
            }

            Game.PlayerCell = new Cell(row, column);
            break;
        }
    }

    static void OnPlayerOption(object sender, PlayerOptionEventArgs e)
    {
        Console.WriteLine();
        if (e.Turn != 1)
        {
            IEnumerable<Cell> PlayedCellsQuery =
                from cell in e.Board
                where cell.Valid == true
                where cell.Value != null
                select cell;
            foreach (var item in e.Board)
            {
                if (item.X == 6)
                    Console.WriteLine();
                if (!item.Valid)
                    Console.Write(" ");
                else if (item.Value != null)
                    Console.Write($"{item.Value}");
                else
                {
                    foreach (var item1 in PlayedCellsQuery)
                    {
                        if (Math.Abs(item.X - item1.X) <= 1 && Math.Abs(item.Y - item1.Y) <= 1)
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write($"{e.Resultat}");
                            Console.ResetColor();
                        }

                    }
                }
            }
            return;
        }
        foreach (var item in e.Board)
        {
            if (!item.Valid)
                Console.Write(" ");
            else
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write($"{e.Resultat}");
                Console.ResetColor();

            }
            if (item.X == 6)
                Console.WriteLine();
        }
        Console.WriteLine();
        Console.ForegroundColor = ConsoleColor.Cyan;
        Console.WriteLine("Choose an Available cell.");
        Console.ResetColor();
    }

    static void OnPlayerSelectionOp(object sender, PlayerChooseOperationEventArgs e)
    {
        Console.WriteLine();
        DisplayOperationTable(((Game)sender).UsedMap.OperationGrid.ToList());
        Console.WriteLine();
        Console.WriteLine("0. Lower |   1. Higher   |   2. Substraction |   3. Addition |   4. Multiplication");
        string? op = Console.ReadLine();
        while (op != "0" && op != "1" && op != "2" && op != "3" && op != "4")
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Invalid operation. Please choose again.");
            Console.ResetColor();
            op = Console.ReadLine();
        }
        int test = Convert.ToInt32(op);
        Game.PlayerOperation = (Operation)test;
        
    }


    /// <summary>
    /// Handles the event when the game has started.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    static void OnGameStarted(object sender, GameStartedEventArgs e)
    {
        Console.WriteLine($"The game has started! Player: {e.CurrentPlayer.Pseudo}");
    }

    /// <summary>
    /// Handles the event when the game has ended.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    static void OnGameEnded(object sender, GameEndedEventArgs e)
    {
        Console.WriteLine($"The game has ended! Player: {e.CurrentPlayer.Pseudo}");
        Console.WriteLine($"Points: {e.Point}");
    }

    /// <summary>
    /// Handles the event when the board is updated.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    static void OnBoardUpdated(object sender, BoardsUpdateEventArgs e)
    {
        foreach (var item in e.Boards)
        {
            if (!item.Valid)
                Console.Write(" ");
            else if (item.Value != null)
                Console.Write($"{item.Value}");
            else Console.Write("O");
            if (item.X == 6)
                Console.WriteLine();
        }
    }

    /// <summary>
    /// Handles the event when the dice are rolled.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    static void OnDiceRolled(object sender, DiceRolledEventArgs e)
    {
       // Console.Clear();
        Console.WriteLine($"Dice 1: {e.Dice1Value} | Dice 2: {e.Dice2Value}");
        Console.WriteLine();
        Console.ForegroundColor = ConsoleColor.Cyan;
        Console.WriteLine("Choose an operation.");
        Console.ResetColor();
    }

    /// <summary>
    /// Handles the event when an operation is chosen by the player.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    static void OnOperationChosen(object sender, OperationChosenEventArgs e)
    {
        Console.WriteLine($"Operation: {e.Operation}, Result: {e.Result}");
        DisplayOperationTable(((Game)sender).UsedMap.OperationGrid.ToList());
        Cell playerChoice = GetPlayerChoice();
        bool test = ((Game)sender).HandlePlayerChoice(playerChoice, e.Result);
        if(!test)
        {
            Console.WriteLine("Invalid cell. Please choose again.");
            Console.WriteLine();
            Console.WriteLine();
            DisplayBoard(((Game)sender).UsedMap);
            OnOperationChosen(sender, e);
        }



        /*
        try
        {
            ((Game)sender).HandlePlayerChoice(playerChoice, e.Result);
        }
        catch (InvalidCellCoordinatesException err)
        {
            Console.WriteLine(err.Message);
            playerChoice = GetPlayerChoice();
            ((Game)sender).HandlePlayerChoice(playerChoice, e.Result);
        }
        catch (InvalidCellException err)
        {
            Console.WriteLine(err.Message);
            playerChoice = GetPlayerChoice();
            ((Game)sender).HandlePlayerChoice(playerChoice, e.Result);
        }
        catch (InvalidPlaceResultException err)
        {
            Console.WriteLine(err.Message);
            playerChoice = GetPlayerChoice();
            ((Game)sender).HandlePlayerChoice(playerChoice, e.Result);
        }
        */
    }

    /// <summary>
    /// Handles the event when a cell is chosen by the player.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    static void OnCellChosen(object sender, CellChosenEventArgs e)
    {
        Console.WriteLine($"Cell chosen: ({e.Cell.X}, {e.Cell.Y}) with result: {e.Result}");
        DisplayBoard(((Game)sender).UsedMap);
    }

    /// <summary>
    /// Displays the game board.
    /// </summary>
    /// <param name="map">Used map to display</param>
    static void DisplayBoard(Map map)
    {
        int cpt = 0;
        for (int i = 0; i < map.Boards.Count; i++)
        {
            if (cpt % 6 == 0)
            {
                Console.Write("| ");
            }

            if (map.Boards[i].Value.HasValue)
            {
                Console.Write(map.Boards[i].Value?.ToString().PadLeft(2));
            }
            else
            {
                Console.Write(" O");
            }

            cpt++;
            if (cpt % 6 == 0)
            {
                Console.Write(" |");
                Console.WriteLine();
            }
        }
    }

    /// <summary>
    /// Displays the operation table.
    /// </summary>
    /// <param name="operationTable">The operation table to display.</param>
    static void DisplayOperationTable(List<OperationCell> operationTable)
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        foreach ( var cell in operationTable)
        {
            
            if (cell.X == 0 && cell.Y == 0)
                Console.Write("Lower  =>    ");
            if (cell.X == 0 && cell.Y == 1)
                Console.Write("Higher  =>    ");
            if (cell.X == 0 && cell.Y == 2)
                Console.Write("Substraction  =>    ");
            if (cell.X == 0 && cell.Y == 3)
                Console.Write("Addition  =>    ");
            if (cell.X == 0 && cell.Y == 4)
                Console.Write("Multiplication  =>    ");
            if (cell.IsChecked)
                Console.Write("X  |  ");
            if (!cell.IsChecked)
                Console.Write("   |  ");
            if (cell.X == 3)
                Console.WriteLine();
        }
        Console.ResetColor();
    }

    /// <summary>
    /// Gets the cell chosen by the player.
    /// </summary>
    /// <returns>The cell chosen by the player.</returns>
    static Cell GetPlayerChoice()
    {
        int row, column;
        while (true)
        {
            Console.WriteLine("Enter the position of the cell you want to play");
            Console.WriteLine("Enter the row number (0-5)");
            if (!int.TryParse(Console.ReadLine(), out row) || row < 0 || row >= 6)
            {
                Console.WriteLine("Invalid row number. Please enter a number between 0 and 5.");
                continue;
            }

            Console.WriteLine("Enter the column number (0-5)");
            if (!int.TryParse(Console.ReadLine(), out column) || column < 0 || column >= 6)
            {
                Console.WriteLine("Invalid column number. Please enter a number between 0 and 5.");
                continue;
            }

            return new Cell(row, column);
        }
    }

    /// <summary>
    /// Gets the operation chosen by the player.
    /// </summary>
    /// <returns></returns>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    static Operation GetPlayerOperation(object? sender)
    {
        DisplayOperationOptions();
        string? op = Console.ReadLine();
        while (op != "1" && op != "2" && op != "3" && op != "4" && op != "5")
        {
            Console.WriteLine("Invalid operation. Please choose again.");
            op = Console.ReadLine();
        }
        int test = Convert.ToInt32(op);
        
        while(((Game)sender).UsedMap.CheckOperationPossible(test-1))
        {
            Console.WriteLine("Invalid operation. Please choose again.");
            Console.WriteLine();
            op = Console.ReadLine();
            while (op != "1" && op != "2" && op != "3" && op != "4" && op != "5")
            {
                Console.WriteLine("Invalid operation. Please choose again.");
                op = Console.ReadLine();
            }
            test = Convert.ToInt32(op);
        }

        return op switch
        {
            "1" => Operation.ADDITION,
            "2" => Operation.SUBTRACTION,
            "3" => Operation.MULTIPLICATION,
            "4" => Operation.LOWER,
            "5" => Operation.HIGHER,
            _ => throw new ArgumentOutOfRangeException()
        };
    }

    /// <summary>
    /// Displays the operation options for the player to choose from.
    /// </summary>
    static void DisplayOperationOptions()
    {
        Console.WriteLine("Choose an operation:");
        Console.WriteLine("1: Addition (+)");
        Console.WriteLine("2: Subtraction (-)");
        Console.WriteLine("3: Multiplication (*)");
        Console.WriteLine("4: Lower Dice (less)");
        Console.WriteLine("5: Higher Dice (high)");
    }
}