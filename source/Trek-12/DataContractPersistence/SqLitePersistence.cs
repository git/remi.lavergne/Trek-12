﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;

namespace DataContractPersistence
{
    using Models.Game;
    using Models.Interfaces;

    public class SqLitePersistence : IPersistence
    {
        private readonly SQLiteConnection _database;
        private readonly string _databasePath;

        public SqLitePersistence()
        {
            _databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Trek_12.db");
            _database = new SQLiteConnection(_databasePath);
            _database.CreateTable<Player>();
            _database.CreateTable<Map>();
            _database.CreateTable<Game>();
            _database.CreateTable<BestScore>();
        }

        public (ObservableCollection<Player>, ObservableCollection<Game>, ObservableCollection<Map>, ObservableCollection<BestScore>) LoadData()
        {
            var players = new ObservableCollection<Player>(_database.Table<Player>());
            var games = new ObservableCollection<Game>(_database.Table<Game>());
            var maps = new ObservableCollection<Map>(_database.Table<Map>());
            var bestScores = new ObservableCollection<BestScore>(_database.Table<BestScore>());

            return (players, games, maps, bestScores);
        }

        public void SaveData(ObservableCollection<Player> players, ObservableCollection<Game> games, ObservableCollection<Map> maps, ObservableCollection<BestScore> bestScores)
        {
            _database.RunInTransaction(() =>
            {
                _database.DeleteAll<Player>();
                _database.DeleteAll<Game>();
                _database.DeleteAll<Map>();
                _database.DeleteAll<BestScore>();
            });

            foreach (var player in players)
            {
                _database.Insert(player);
            }

            foreach (var game in games)
            {
                _database.Insert(game);
            }

            foreach (var map in maps)
            {
                _database.Insert(map);
            }

            foreach (var bestScore in bestScores)
            {
                _database.Insert(bestScore);
            }
        }
    }
}
