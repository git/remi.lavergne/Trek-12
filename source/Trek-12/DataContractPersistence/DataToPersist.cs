﻿using System.Collections.ObjectModel;
using Models.Game;

namespace DataContractPersistence
{
    public class DataToPersist
    {
        /// <summary>
        /// List of players
        /// </summary>
        public ObservableCollection<Player> Players { get; set; }

        /// <summary>
        /// List of games (with all the data in it to be able to recreate the game)
        /// </summary>
        public ObservableCollection<Game> Games { get; set; }

        /// <summary>
        /// List of maps with their boards
        /// </summary>
        public ObservableCollection<Map> Maps { get; set; }
        
        /// <summary>
        /// List of best scores
        /// </summary>
        public ObservableCollection<BestScore> BestScores { get; set; }
    }
}