﻿using System.Diagnostics;
using System.Runtime.Serialization.DataContracts;


namespace Trek_12
{
    using Stub;
    using Models.Game;
    using Models.Interfaces;
    using DataContractPersistence;

    public partial class App : Application
    {
        public string FileName { get; set; } = "data.json";

        public string FilePath { get; set; } = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Trek_12");

        public Game Manager { get; private set; }

        public App()
        {
            InitializeComponent();

            Manager = new Game(new DataContractJson());
            //Manager = new Game(new Stub());

            if (!Directory.Exists(FilePath))
            {
                Directory.CreateDirectory(FilePath);
            }

            //File.Delete(Path.Combine(FilePath, FileName));

            string fullPath = Path.Combine(FilePath, FileName);
            if (File.Exists(fullPath))
            {
                Debug.WriteLine("Data loaded from " + fullPath);
                Manager.LoadData();
            }
            /* Add the permanent maps if they are not already in the game */
            if (Manager.Maps.Count == 0)
            {
                Manager.AddMap(new Map("Dunai","montagne2.png"));
                Manager.AddMap(new Map("Kagkot", "montagne3.png"));
                Manager.AddMap(new Map("Dhaulagiri", "montagne4.png"));
            }


            MainPage = new AppShell();

            // If the MainPage is closed, we save the data
            MainPage.Disappearing += (sender, e) =>
            {
                Debug.WriteLine("Saving data...");
                Manager.SaveData();
            };

        }

        /// <summary>
        /// Save the data when the app is in background
        /// </summary>
        protected override void OnSleep()
        {
            Debug.WriteLine("Zzz Secure save...");
            Manager.SaveData();
        }
    }
}
