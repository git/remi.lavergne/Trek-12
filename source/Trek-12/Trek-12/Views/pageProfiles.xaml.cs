using Models.Game;
using Stub;
using System.Diagnostics;
using CommunityToolkit.Maui.Alerts;
using CommunityToolkit.Maui.Core;
using Models.Interfaces;

namespace Trek_12.Views;

public partial class PageProfiles : ContentPage
{
    public Game ProfileManager => (App.Current as App).Manager;

    public PageProfiles()
	{
		InitializeComponent();
		BindingContext = ProfileManager;
	}

    private async void OnBackArrow_Tapped(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync("..");
    }

    async void Button_ClickedAdd(System.Object sender, System.EventArgs e)
    {
        string pseudo = await DisplayPromptAsync("Info", $"Choose a name : ", "Ok");
        char[] trim = { ' ', '\n', '\t' };
        pseudo = pseudo.TrimEnd(trim);
        pseudo = pseudo.TrimStart(trim);
        if (pseudo == null) return;
        if (ProfileManager.Players.Any(p => p.Pseudo == pseudo))
        {
            await DisplayAlert("Info", "This name is already taken", "Ok");
            return;
        }

        var profilePicture = await MediaPicker.PickPhotoAsync();
        if (profilePicture == null) return;

        IImageConverter converter = new Base64Converter();

        string convertedProfilePicture = converter.ConvertImage(profilePicture.FullPath);

        Player player = new Player(pseudo, convertedProfilePicture);
        ProfileManager.AddPlayer(player);
        Debug.WriteLine("Player " + pseudo + " added with profile picture " + convertedProfilePicture);
        Debug.WriteLine("It's the number" + ProfileManager.Players.Count + " player");

        ProfileManager.OnPropertyChanged(nameof(ProfileManager.Players));
        ProfileManager.SaveData();
    }

    async void Button_ClickedPop(System.Object sender, System.EventArgs e)
    {
        if (ProfileManager.Players.Count == 0)
        {
            await DisplayAlert("Info", "There is no player actually\nPlease add one", "Ok");
            return;
        }

        string result = await DisplayPromptAsync("Info", $"Choose a pseudo to delete : ", "Ok");
        if (result == null) return;
        Debug.WriteLine("Answer: " + result);
        if (ProfileManager.RemovePlayer(result))
        {
            Debug.WriteLine("bam, deleted");
            OnPropertyChanged(nameof(ProfileManager));
        }
        else
        {
            await DisplayAlert("Info", "This name do not exist", "Ok");
            Debug.WriteLine("Player not found");
            return;
        }
        

    }

    async void Button_ClickedModify(System.Object sender, System.EventArgs e)
    {
        if (ProfileManager.Players.Count == 0)
        {
            await DisplayAlert("Info", "There is no player actually\nPlease add one", "Ok");
            return;
        }

        string result = await DisplayPromptAsync("Info", $"Choose a name to modify : ", "Ok");
        Debug.WriteLine("Answer: " + result);
        if (result == null)
        {
            Debug.WriteLine("Did not found");
            return;
        }
        string tomodify = await DisplayPromptAsync("Info", $"How will you rename it ?: ", "Ok");
        Debug.WriteLine("Answer: " + tomodify);
        if (tomodify == null)
        {
            Debug.WriteLine("Did not found");
            return;
        }
        Debug.WriteLine("bam, modified");
        bool ismodified = ProfileManager.ModifyPlayer(result, tomodify);

        if (ismodified)
        {
            Debug.WriteLine("Modified");
            ProfileManager.SaveData();
        }
        else Debug.WriteLine("Player not found");
    }
}

