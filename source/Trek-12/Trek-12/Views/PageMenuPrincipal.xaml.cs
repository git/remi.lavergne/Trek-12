﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CommunityToolkit.Maui.Alerts;
using CommunityToolkit.Maui.Core;
using Font = Microsoft.Maui.Font;

namespace Trek_12.Views;

public partial class PageMenuPrincipal : ContentPage
{
    public PageMenuPrincipal()
    {
        InitializeComponent();
    }

    private async void OnRulesButton_Clicked(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync(nameof(PageRegles));
    }

    private async void OnLeaderBoardButton_Clicked(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync(nameof(PageLeaderBoard));
    }

    private async void OnPlayButton_Clicked(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync(nameof(PageSelectMap));
    }

    private async void OnProfilesButton_Tapped(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync(nameof(PageProfiles));
    }
}