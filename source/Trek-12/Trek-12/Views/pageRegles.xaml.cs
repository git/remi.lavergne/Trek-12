using System.ComponentModel;

namespace Trek_12.Views;

public partial class PageRegles : ContentPage
{
	public PageRegles()
	{
		InitializeComponent();
	}

    private async void BackButton_Clicked(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync("..");
    }
    
    private async void BrowserOpen_Clicked(System.Object sender, System.EventArgs e)
    {
        try
        {
            Uri uri = new Uri("https://www.philibertnet.com/fr/lumberjacks-studio/89702-trek-12-3760268310293.html");
            await Browser.Default.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
        }
        catch (Exception)
        {
            // An unexpected error occurred. No browser may be installed on the device.
            throw new NotImplementedException();
        }
    }
}
