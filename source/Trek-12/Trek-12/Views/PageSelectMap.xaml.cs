
using System.Diagnostics;

namespace Trek_12.Views;
using Stub;
using Models.Game;

public partial class PageSelectMap : ContentPage
{
    public Game SelectMapManager => (App.Current as App).Manager;
    private Map? _selectedMap;

    private bool isVisibleContinueButton = false;

    protected override async void OnAppearing()
    {
        base.OnAppearing();
        if (SelectMapManager.Games.Any(g => g.IsRunning))
        {
            isVisibleContinueButton = true;
            await DisplayAlert("Warning", "You've previously quit in the middle of a game.\nIf you start a new game, this one will be permanently lost.", "I understand");

        }
    }

    public PageSelectMap()
	{
		InitializeComponent();
		BindingContext = SelectMapManager;

	}

    private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
        _selectedMap = e.CurrentSelection.FirstOrDefault() as Map;
    }

    private async void BackButton_Clicked(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync("..");
    }

    private async void PlayButton_Clicked(object sender, EventArgs e)
    {
        if (_selectedMap == null)
        {
            await DisplayAlert("Selection Required", "Please select a map you want to play to continue.", "OK");
            return;
        }
        if (SelectMapManager.Players.Count == 0)
        {
            await DisplayAlert("No player found", "Please add a player in the profile page.", "OK");
            return;
        }

        string[] profiles = GetProfiles().ToArray();
        string choosenPlayerName = await DisplayActionSheet("Choose a player", "Cancel", null, profiles);
        if (choosenPlayerName == null || choosenPlayerName == "Cancel") return;

        Player chosenPlayer = GetProfileByName(choosenPlayerName);

        var runningGames = SelectMapManager.Games.Where(g => g.IsRunning).ToList();

        bool delete = false;
        foreach (var game in runningGames)
        {
            SelectMapManager.Games.Remove(game);
            delete = true;
        }

        if (delete)
        {
            await DisplayAlert("Game deleted", "The previous game has been deleted because you started a new one.", "OK");
            SelectMapManager.OnPropertyChanged(nameof(SelectMapManager.Games));
            SelectMapManager.SaveData();
        }
        

        SelectMapManager.InitializeGame(_selectedMap.Clone(), chosenPlayer);

        if (SelectMapManager.UsedMap != null && Equals(SelectMapManager.CurrentPlayer, chosenPlayer))
        {
            await Shell.Current.GoToAsync(nameof(PageBoard));
        }
        else
        {
            await DisplayAlert("Error", "An error occured while initializing the game. Please try again.", "OK");
        }
    }

    private List<string> GetProfiles()
    {
        return SelectMapManager.Players.Select(p => p.Pseudo).ToList();
    }

    private Player GetProfileByName(string pseudo)
    {
        return SelectMapManager.Players.FirstOrDefault(p => p.Pseudo == pseudo);
    }

    private async void ResumeButton_Clicked(object sender, EventArgs e)
    {
        Game game = SelectMapManager.Games.FirstOrDefault(g => g.IsRunning);
        if (game == null)
        {
            await DisplayAlert("No game found", "No game found to resume. Please start a new game.", "OK");
            return;
        }

        SelectMapManager.InitializeGame(game.UsedMap, game.CurrentPlayer, false);
        await Shell.Current.GoToAsync(nameof(PageBoard));
    }

}