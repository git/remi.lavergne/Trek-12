using Models.Game;
using System.Diagnostics;

namespace Trek_12.Views;

public partial class PageLeaderBoard : ContentPage
{
    public Game LeaderboardManager => (App.Current as App).Manager;
    
    public PageLeaderBoard()
    {
        InitializeComponent();
        BindingContext = LeaderboardManager;
    }

    private async void OnBackArrow_Tapped(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync("..");
    }
}