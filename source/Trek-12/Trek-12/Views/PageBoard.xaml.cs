using System.ComponentModel;
using System.Diagnostics;
using Microsoft.VisualBasic;

namespace Trek_12.Views;

using Models.Events;
using Models.Game;

public partial class PageBoard : ContentPage
{
    public Game GameManager => (App.Current as App).Manager;

    public int Result {  get; set; }

    public Cell ChoosenCell { get; set; }

    public PageBoard()
	{
		InitializeComponent();
        BindingContext = GameManager;
        GameManager.CurrentPlayer.UpdateLastPlayed();

        GameManager.DiceRolled += TheGame_DiceRolled;
        GameManager.DiceRolled += ResultAddition;
        GameManager.DiceRolled += ResultLower;
        GameManager.DiceRolled += ResultHigher;
        GameManager.DiceRolled += ResultSubstraction;
        GameManager.DiceRolled += ResultMultiplication;
        GameManager.PlayerOption += GameManager_PlayerOption;
        GameManager.CellChosen += HandleCellChosen;

        GameManager.AddGame(GameManager);
        GameManager.OnPropertyChanged(nameof(GameManager.Games));
        GameManager.SaveData();
    }

    private void HandleCellChosen(object sender, CellChosenEventArgs e)
    {
        YellowDice.IsVisible = false;
        RedDice.IsVisible = false;
        RollButton.IsEnabled = true;
    }

    private void ResetOperationButtonsAndDice()
    {
        Lower.IsVisible = false;
        Higher.IsVisible = false;
        Substraction.IsVisible = false;
        Addition.IsVisible = false;
        Multiplication.IsVisible = false;

        RollButton.IsEnabled = true;
        YellowDice.IsVisible = false;
        RedDice.IsVisible = false;
    }

    private void SetOperationButtonState(Button selectedButton)
    {
        // Deselect all buttons
        Lower.BackgroundColor = Colors.DarkSalmon;
        Higher.BackgroundColor = Colors.DarkSalmon;
        Substraction.BackgroundColor = Colors.DarkSalmon;
        Addition.BackgroundColor = Colors.DarkSalmon;
        Multiplication.BackgroundColor = Colors.DarkSalmon;

        // Select the clicked button
        selectedButton.BackgroundColor = Colors.LightCoral;
    }

    private void GameManager_PlayerOption(object? sender, PlayerOptionEventArgs e)
    {
        /* IEnumerable<Cell> PlayedCellsQuery =
                 from cell in e.Board
                 where cell.Valid == true
                 where cell.Value != null
                 select cell;*/

        // prévisualisation des zone disponible, Je ne sais pas comment ca marche... 😵

    }

    private void ResultMultiplication(object? sender, DiceRolledEventArgs e)
    {
        Multiplication.IsVisible = true;
        Multiplication.Text = $"Mult {e.Dice1Value*e.Dice2Value}";
    }

    private void ResultSubstraction(object? sender, DiceRolledEventArgs e)
    {
        Substraction.IsVisible = true;
        if (GameManager.Dice1.IsLower(GameManager.Dice2))
            Substraction.Text = $"Sub {e.Dice2Value - e.Dice1Value}";
        else Substraction.Text = $"Sub {e.Dice1Value - e.Dice2Value}";
    }

    private void ResultHigher(object? sender, DiceRolledEventArgs e)
    {
        Higher.IsVisible = true;
        if (GameManager.Dice1.IsLower(GameManager.Dice2))
            Higher.Text = $"Higher {e.Dice2Value}";
        else Higher.Text = $"Higher {e.Dice1Value}";
    }

    private void ResultLower(object? sender, DiceRolledEventArgs e)
    {
        Lower.IsVisible = true;
        if(GameManager.Dice1.IsLower(GameManager.Dice2))
            Lower.Text = $"Lower {e.Dice1Value}";
        else Lower.Text = $"Lower {e.Dice2Value}";
    }

    private void ResultAddition(object? sender, DiceRolledEventArgs e)
    {
        Addition.IsVisible = true;
        Addition.Text = $"Add {e.Dice1Value+e.Dice2Value}";
    }

    private void TheGame_DiceRolled(object? sender, Models.Events.DiceRolledEventArgs e)
    {
        YellowDice.IsVisible = true;
        RedDice.IsVisible = true;
        Dice1.Text = $"{e.Dice1Value}";
        Dice2.Text = $"{e.Dice2Value}";
        RollButton.IsEnabled = false;
    }

    private void OnOperationCellSelected(object sender, SelectionChangedEventArgs e)
    {
        if (e.CurrentSelection.Count > 0) // Si un élément est sélectionné
        {
            var selectedCell = (OperationCell)e.CurrentSelection[0];
            if (selectedCell != null && !selectedCell.IsChecked)
            {
                selectedCell.Check();
                Debug.WriteLine("OperationCell at ({0}, {1}) is checked", selectedCell.X, selectedCell.Y); // Debug
            }
            ((CollectionView)sender).SelectedItem = null; // Déselectionne l'élément pour la CollectionView
        }
    }

    private void HigherClicked(object sender, EventArgs e)
    {
        GameManager.PlayerOperation = Operation.HIGHER;
        SetOperationButtonState((Button)sender);
        Result = GameManager.ResultOperation(Operation.HIGHER);
        GameManager.HandlePlayerOperation(Operation.HIGHER);
    }

    private void LowerClicked(object sender, EventArgs e)
    {
        GameManager.PlayerOperation = Operation.LOWER;
        SetOperationButtonState((Button)sender);
        Result = GameManager.ResultOperation(Operation.LOWER);
        GameManager.HandlePlayerOperation(Operation.LOWER);
    }

    private void AdditionClicked(object sender, EventArgs e)
    {
        GameManager.PlayerOperation = Operation.ADDITION;
        SetOperationButtonState((Button)sender);
        Result = GameManager.ResultOperation(Operation.ADDITION);
        GameManager.HandlePlayerOperation(Operation.ADDITION);
    }

    private void SubstractionClicked(object sender, EventArgs e)
    {
        GameManager.PlayerOperation = Operation.SUBTRACTION;
        SetOperationButtonState((Button)sender);
        Result = GameManager.ResultOperation(Operation.SUBTRACTION);
        GameManager.HandlePlayerOperation(Operation.SUBTRACTION);
    }

    private void MultiplicationClicked(object sender, EventArgs e)
    {
        GameManager.PlayerOperation = Operation.MULTIPLICATION;
        SetOperationButtonState((Button)sender);
        Result = GameManager.ResultOperation(Operation.MULTIPLICATION);
        GameManager.HandlePlayerOperation(Operation.MULTIPLICATION);
    }

    private void DiceButton_Clicked(object sender, EventArgs e)
    {
        GameManager.RollAllDice();
    }

    private async void OnCellSelected(object sender, SelectionChangedEventArgs e)
    {
        if (!GameManager.DiceRolledFlag)
        {
            await DisplayAlert("Action Required", "You must roll the dice first.", "OK");
            return;
        }

        if (!GameManager.OperationChosenFlag)
        {
            await DisplayAlert("Action Required", "You must choose an operation first.", "OK");
            return;
        }

        if (e.CurrentSelection.Count > 0)
        {
            ChoosenCell = (Cell)e.CurrentSelection[0];
            GameManager.PlayerCell = ChoosenCell;
            GameManager.Resultat = Result;
            OnPropertyChanged(nameof(GameManager.PlayerCell));
            OnPropertyChanged(nameof(GameManager.Resultat));

            GameManager.PlayerSelectionCell();

            ((CollectionView)sender).SelectedItem = null;

            ResetOperationButtonsAndDice();
        }
    }
}