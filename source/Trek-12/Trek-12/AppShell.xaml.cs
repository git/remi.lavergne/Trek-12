﻿using Trek_12.Views;

namespace Trek_12
{
    /// <summary>
    /// Class for the route of the views and the navigation of the app.
    /// </summary>
    public partial class AppShell : Shell
    {
        public AppShell()
        {
            InitializeComponent();
            
            Routing.RegisterRoute(nameof(PageMenuPrincipal), typeof(PageMenuPrincipal));
            Routing.RegisterRoute(nameof(PageProfiles), typeof(PageProfiles));
            Routing.RegisterRoute(nameof(PageSelectMap), typeof(PageSelectMap));
            Routing.RegisterRoute(nameof(PageRegles), typeof(PageRegles));
            Routing.RegisterRoute(nameof(PageLeaderBoard), typeof(PageLeaderBoard));
            Routing.RegisterRoute(nameof(PageBoard), typeof(PageBoard));
        }
    }
}
