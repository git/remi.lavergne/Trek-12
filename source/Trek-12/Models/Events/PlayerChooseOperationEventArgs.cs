﻿using Models.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Events
{
    public class PlayerChooseOperationEventArgs : EventArgs
    {
        public Operation PlayerOp { get; set; }

        public PlayerChooseOperationEventArgs(Operation op)
        {
            PlayerOp = op;
        }
    }
}
