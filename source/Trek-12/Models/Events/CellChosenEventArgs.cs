﻿using Models.Game;

namespace Models.Events
{
    /// <summary>
    /// Event arguments for when a cell is chosen.
    /// </summary>
    public class CellChosenEventArgs : EventArgs
    {
        public Cell Cell { get; }
        public int Result { get; }

        public CellChosenEventArgs(Cell cell, int result)
        {
            Cell = cell;
            Result = result;
        }
    }
}