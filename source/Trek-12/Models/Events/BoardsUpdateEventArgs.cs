﻿using Models.Game;

namespace Models.Events
{
    /// <summary>
    /// Event arguments for when the board change.
    /// </summary>
    public class BoardsUpdateEventArgs : EventArgs
    {
        public List<Cell> Boards { get; set; }

        public BoardsUpdateEventArgs(List<Cell> board)
        {
            Boards = board;
        }
    }
}
