﻿using Models.Game;

namespace Models.Events
{
    /// <summary>
    /// Event arguments for when an operation is chosen.
    /// </summary>
    public class OperationChosenEventArgs : EventArgs
    {
        public Operation Operation { get; }
        public int Result { get; }

        public OperationChosenEventArgs(Operation operation, int result)
        {
            Operation = operation;
            Result = result;
        }
    }
}