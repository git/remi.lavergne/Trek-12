﻿using Models.Game;

namespace Models.Events
{
    /// <summary>
    /// Event arguments for when the game starts.
    /// </summary>
    public class GameStartedEventArgs : EventArgs
    {
        public Player CurrentPlayer { get; }

        public GameStartedEventArgs(Player selectedPlayer)
        {
            CurrentPlayer = selectedPlayer;
        }
    }
}

