﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Game;

namespace Models.Events
{
    public class PlayerOptionEventArgs : EventArgs
    {
        public List<Cell> Board { get; set; }
        public int Resultat {  get; set; }
        public int Turn { get; set; }

        public PlayerOptionEventArgs(List<Cell> boards, int resultat, int turn)
        {
            Board = boards;
            Resultat = resultat;
            Turn = turn;
        }
    }
}
