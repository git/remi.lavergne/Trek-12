﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Game;

namespace Models.Events
{
    public class PlayerChooseCellEventArgs : EventArgs
    {
        public Cell Cell { get; set; }
        public PlayerChooseCellEventArgs(Cell cell)
        {
            Cell = cell;
        }
    }
}
