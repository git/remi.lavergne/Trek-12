﻿using Models.Game;

namespace Models.Events
{
    /// <summary>
    /// Event arguments for when the dice are rolled.
    /// </summary>
    public class DiceRolledEventArgs : EventArgs
    {
        public int Dice1Value { get; }
        public int Dice2Value { get; }

        public DiceRolledEventArgs(int dice1Value, int dice2Value)
        {
            Dice1Value = dice1Value;
            Dice2Value = dice2Value;
        }
    } 
}
