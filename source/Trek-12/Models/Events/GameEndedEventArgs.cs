﻿using Models.Game;

namespace Models.Events
{
    /// <summary>
    /// Event arguments for when the game ends.
    /// </summary>
    public class GameEndedEventArgs : EventArgs
    {
        public Player CurrentPlayer { get; }

        public int? Point { get; }

        public GameEndedEventArgs(Player winner, int? point)
        {
            CurrentPlayer = winner;
            Point = point;
        }
    }
}
