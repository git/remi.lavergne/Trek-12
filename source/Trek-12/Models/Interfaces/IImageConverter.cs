﻿using Microsoft.Maui.Controls;

namespace Models.Interfaces
{
    /// <summary>
    /// Interface for image converters
    /// </summary>
    public interface IImageConverter
    {
        /// <summary>
        /// Converter to any type that can be converted to a string (ex: Base64)
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        string ConvertImage(string imagePath);

        /// <summary>
        /// Retrieve an image from a string encoded
        /// </summary>
        /// <param name="imageString"></param>
        /// <returns></returns>
        ImageSource RetrieveImage(string imageString);
    }
}
