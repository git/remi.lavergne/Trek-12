﻿using System.Collections.ObjectModel;

namespace Models.Interfaces
{
    using Models.Game;
    public interface IPersistence
    {
        (ObservableCollection<Player>, ObservableCollection<Game>, ObservableCollection<Map>, ObservableCollection<BestScore>) LoadData();
        
        void SaveData(ObservableCollection<Player> players, ObservableCollection<Game> games, ObservableCollection<Map> maps, ObservableCollection<BestScore> bestScores);
    }
}