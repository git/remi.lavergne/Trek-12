﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Game
{
    /// <summary>
    /// Represents the available operations in the game.
    /// </summary>
    public enum Operation
    {
        LOWER,
        HIGHER,
        SUBTRACTION,
        ADDITION,
        MULTIPLICATION
        
        
    }
}
