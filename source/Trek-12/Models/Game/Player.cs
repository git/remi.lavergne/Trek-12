﻿using SQLite;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using Models.Interfaces;

namespace Models.Game
{

    /// <summary>
    /// Represents a player in the game.
    /// </summary>
    [DataContract, SQLite.Table("Players")]
    public class Player : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));


        /// <summary>
        /// It is he pseudo of the player.
        /// </summary>
        private string _pseudo;
        [DataMember, PrimaryKey]
        public string Pseudo
        {
            get => _pseudo;
            set
            {
                if (_pseudo == value)
                    return;
                _pseudo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// It is the profile picture of the player.
        /// </summary>
        [DataMember]
        public string ProfilePicture { get; private set; }

        /// <summary>
        /// It is the creation date of the player.
        /// </summary>
        [DataMember]
        public string CreationDate { get; private set; }

        /// <summary>
        /// It tells when was the last time the player played.
        /// </summary>
        [DataMember]
        public string? LastPlayed { get; private set; }

        /// <summary>
        /// Construct a new instance of Player with specified pseudo and profile picture.
        /// </summary>
        /// <param name="pseudo">The pseudo of the player.</param>
        /// <param name="profilePicture">The profile picture of the player.</param>
        public Player(string pseudo, string profilePicture)
        {
            Pseudo = pseudo;
            ProfilePicture = profilePicture;
            CreationDate = DateTime.Now.ToString("dd/MM/yyyy");
        }

        /// <summary>
        /// SQLite constructor
        /// </summary>
        public Player() { }

        /// <summary>
        /// Redefine the equal operation between player.
        /// </summary>
        /// <param name="obj">The object to compare with the current player.</param>
        /// <returns>true if the specified object is equal to the current player; otherwise, false.</returns>
        public override bool Equals(object? obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            Player c = (Player)obj;
            return (Pseudo == c.Pseudo);
        }

        /// <summary>
        /// Returns the hash code for the current player, in order for the Equals operation to work
        /// </summary>
        /// <returns>The hash code for the current player.</returns>
        public override int GetHashCode()
        {
            return Pseudo.GetHashCode();
        }
        
        /// <summary>
        /// Actualize the last time the player played.
        /// </summary>
        public void UpdateLastPlayed()
        {
            LastPlayed = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }
}