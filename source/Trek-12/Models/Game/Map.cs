﻿using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using SQLite;

namespace Models.Game
{

    /// <summary>
    /// The Map class is the representation of the game map with the board and the operations table.
    /// </summary>
    [DataContract, SQLite.Table("Maps")]
    public class Map
    {
        /// <summary>
        /// The displaying name of the map
        /// </summary>
        [DataMember, PrimaryKey]
        public string Name { get; private set; }

        /// <summary>
        /// It is the list of cells on the map.
        /// </summary>
        [DataMember]
        [Ignore]
        public ReadOnlyObservableCollection<Cell> Boards { get; private set; }

        private readonly ObservableCollection<Cell> _board = new ObservableCollection<Cell>();

        /// <summary>
        /// It is the backgrond image of the map
        /// </summary>
        [DataMember]
        public string Background { get; private set; }

        /// <summary>
        /// It is the grid of the possible operation in the game
        /// </summary>
        [DataMember]
        public ReadOnlyObservableCollection<OperationCell> OperationGrid { get; private set; }

        private readonly ObservableCollection<OperationCell> _operationGrid = new ObservableCollection<OperationCell>();

        /// <summary>
        /// It is a list of a list containing user's rope paths in the current game
        /// </summary>
        [DataMember]
        public List<List<Cell>> RopePaths { get; private set; }

        /// <summary>
        /// It is a list of a list containing user's zones in the current game
        /// </summary>
        [DataMember]
        public List<List<Cell>> Zones { get; private set; }

        /// <summary>
        /// Initializes a new instance of the Map class.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="background">The background of the map.</param>
        public Map(string name, string background)
        {
            Name = name;
            Background = background;
            Boards = new ReadOnlyObservableCollection<Cell>(_board);
            InitializeBoards(_board);
            OperationGrid = new ReadOnlyObservableCollection<OperationCell>(_operationGrid);
            InitializeOperationGrid(_operationGrid);
            RopePaths = new List<List<Cell>>();
            Zones = new List<List<Cell>>();
        }
        
        /// <summary>
        /// SQLite constructor
        /// </summary>
        public Map() { }

        /// <summary>
        /// Clone the map to avoid reference problems.
        /// </summary>
        /// <returns></returns>
        public Map Clone()
        {
            Map map = new Map(Name, Background);
            return map;
        }

        /// <summary>
        /// Initializes the boards of the map.
        /// </summary>
        /// <returns>Return the boards</returns>
        private void InitializeBoards(ObservableCollection<Cell> board)
        {
            for (int i = 0; i < 49; i++) // 7x7 board
            {
                board.Add(new Cell(i % 7, i / 7));
            }
            board[1].Valid = true;
            board[3].Valid = true;
            board[4].Valid = true;
            board[5].Valid = true;
            board[9].Valid = true;
            board[12].Valid = true;
            board[15].Valid = true;
            board[16].Valid = true;
            board[17].Valid = true;
            board[21].Valid = true;
            board[24].Valid = true;
            board[25].Valid = true;
            board[28].Valid = true;
            board[30].Valid = true;
            board[31].Valid = true;
            board[32].Valid = true;
            board[40].Valid = true;
            board[41].Valid = true;
            board[48].Valid = true;

            board[3].IsDangerous = true;
            board[15].IsDangerous = true;
            board[16].IsDangerous = true;
        }
        
        /// <summary>
        /// Initializes the operation grid of the map.
        /// </summary>
        /// <returns>Return the operation grid</returns>
        private void InitializeOperationGrid(ObservableCollection<OperationCell>operationGrid)
        {
            for (int i = 0; i < 5; i++) // 5 operations
            {
                for (int j = 0; j < 4; j++) // 4 cells per operation
                {
                    operationGrid.Add(new OperationCell(j, i));
                }
            }
        }

        public bool CheckOperationPossible(int x)
        {
            return OperationGrid[x * 4 + 3].IsChecked;
        }

        public bool IsCellInZones(Cell cell)
        {
            return Zones.Any(zone => zone.Contains(cell));
        }

        public bool IsCellInRopePath(Cell cell)
        {
            return RopePaths.Any(path => path.Contains(cell));
        }
    }
}