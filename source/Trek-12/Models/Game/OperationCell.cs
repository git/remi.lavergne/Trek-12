using System.ComponentModel;
using System.Runtime.Serialization;

namespace Models.Game
{
    /// <summary>
    /// Represents a cell in the operation grid of the game.
    /// </summary>
    [DataContract]
    public class OperationCell : Position, INotifyPropertyChanged
    {
        /// <summary>
        /// It tells if the operation is checked or not in the operation grid of  the game.
        /// </summary>
        [DataMember]
        private bool isChecked;
        public bool IsChecked
        {
            get => isChecked;
            set
            {
                if (isChecked == value)
                    return;
                isChecked = value;
                OnPropertyChanged(nameof(IsChecked));
            }
        }

        /// <summary>
        /// Constructor of the OperationCell class.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public OperationCell(int x, int y) : base(x, y)
        {
            IsChecked = false;
        }

        public event PropertyChangedEventHandler? PropertyChanged;

        void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Check the operation cell.
        /// </summary>
        public void Check()
        {
            IsChecked = true;
        }
    }
}