﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace Models.Game
{

    /// <summary>
    /// The Position (x,y) of a cell in the game.
    /// </summary>
    [DataContract]
    public class Position
    {
        /// <summary>
        /// The X coordinate.
        /// </summary>
        [DataMember]
        public int X { get; set; }

        /// <summary>
        /// The Y coordinate.
        /// </summary>
        [DataMember]
        public int Y { get; set; }

        /// <summary>
        /// Constructor of the Position class.
        /// </summary>
        /// <param name="x">The X coordinate.</param>
        /// <param name="y">The Y coordinate.</param>
        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}