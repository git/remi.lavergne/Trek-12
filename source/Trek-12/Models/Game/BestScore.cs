﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace Models.Game
{
    /// <summary>
    /// This class represents the best score of a player.
    /// </summary>
    [DataContract, SQLite.Table("BestScores")]
    public class BestScore
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        /// <summary>
        /// Name of the map.
        /// </summary>
        [DataMember]
        public string MapName { get; private set; }

        [DataMember]
        public Player ThePlayer { get; private set; }

        /// <summary>
        /// Number of games played by the user (on a specific map).
        /// </summary>
        private int _gamesPlayed;
        [DataMember]
        public int GamesPlayed
        {
            get => _gamesPlayed;
            private set
            {
                if (value >= 0)
                    _gamesPlayed = value;
                else _gamesPlayed = 0;
            }
        }
        
        /// <summary>
        /// Best score of the player (on a specific map).
        /// </summary>
        private int _score;
        [DataMember]
        public int Score
        {
            get => _score;
            private set
            {
                if (value >= 0)
                    _score = value;
                else _score = 0;
            }
        }

        /// <summary>
        /// Initialize a new instance of the BestScore class.
        /// </summary>
        /// <param name="gamesPlayed">Number of games played by the new user</param>
        /// <param name="score">Best score</param>
        /// <param name="map">The name of the map</param>
        public BestScore(string map, Player player, int gamesPlayed, int score)
        {
            MapName = map;
            ThePlayer = player;
            GamesPlayed = gamesPlayed;
            Score = score;
        }

        /// <summary>
        /// SQLite constructor
        /// </summary>
        public BestScore() { }

        /// <summary>
        /// Increment the number of games played by the user.
        /// </summary>
        public void IncrGamesPlayed()
        {
            GamesPlayed += 1;
        }

        /// <summary>
        /// Update the best score of the player.
        /// </summary>
        /// <param name="newScore">New best score</param>
        public void UpdateScore(int newScore)
        {
            Score += newScore;
        }

        /// <summary>
        /// Redefine the equal operation between BestScore.
        /// </summary>
        /// <param name="obj">The object to compare with the current BestScore.</param>
        /// <returns>true if the specified object is equal to the current BestScore; otherwise, false.</returns>
        public override bool Equals(object? obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            BestScore c = (BestScore)obj;
            return (MapName == c.MapName && ThePlayer == c.ThePlayer);
        }

        /// <summary>
        /// Returns the hash code for the current BestScore, in order for the Equals operation to work
        /// </summary>
        /// <returns>The hash code for the current BestScore.</returns>
        public override int GetHashCode()
        {
            return MapName.GetHashCode() ^ ThePlayer.GetHashCode();
        }
    }
}
