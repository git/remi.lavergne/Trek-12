﻿using System;
using Microsoft.Maui.Controls;
using Models.Interfaces;

namespace Models.Game
{
    /// <summary>
    /// Converter to Base64
    /// </summary>
    public class Base64Converter : IImageConverter
    {
        /// <summary>
        /// Converts an image to a base64 string
        /// </summary>
        /// <param name="imagePath">The path to access the image</param>
        /// <returns>The base64 string representation of the image</returns>
        /// <exception cref="FileNotFoundException">Native .NET exception</exception>
        public string ConvertImage(string imagePath)
        {
            if (!File.Exists(imagePath))
            {
                // native .NET exception
                throw new FileNotFoundException("Image file not found", imagePath);
            }

            byte[] imageBytes = File.ReadAllBytes(imagePath);
            return Convert.ToBase64String(imageBytes);
        }

        /// <summary>
        /// Retrieve an image from a string encoded in base64
        /// </summary>
        /// <param name="imageString"></param>
        /// <returns></returns>
        public ImageSource RetrieveImage(string imageString)
        {
            byte[] imageBytes = Convert.FromBase64String(imageString);
            return ImageSource.FromStream(() => new MemoryStream(imageBytes));
        }
    }
}
