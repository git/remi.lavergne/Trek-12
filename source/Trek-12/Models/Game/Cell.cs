﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace Models.Game
{
    /// <summary>
    /// The Cell class represents a cell in the application.
    /// </summary>
    [DataContract]
    public class Cell : Position, IEquatable<Cell>, INotifyPropertyChanged
    {
        /// <summary>
        /// The value of the cell.
        /// </summary>
        private int? _value;
        [DataMember]
        public int? Value {
            get => _value;
            set
            {
                if (value < 0)
                {
                    throw new Exception("La valeur doit être supérieure à 0");
                }
                this._value = value;

                OnPropertyChanged();
            }
        }

        /// <summary>
        /// The fact that the cell is dangerous or not.
        /// </summary>
        [DataMember]
        public bool IsDangerous { get; set; }

        [DataMember]
        public bool Valid { get; set; }


        /// <summary>
        /// Atribute to know if the cell is a penalty cell.
        /// </summary>
        [DataMember]
        public bool Penalty { get; private set; }

        public void SetPenalty()
        {
           Penalty = true;
        }

        /// <summary>
        /// Constructor of the Cell class.
        /// </summary>
        /// <param name="x">the x position</param>
        /// <param name="y">the y position</param>
        /// <param name="isDangerous">if the cell is a dangerous cell or not</param>
        public Cell(int x, int y,bool isDangerous = false):base(x,y)
        {
            IsDangerous = isDangerous;
            Penalty = false;
            Valid = false;
        }

        /// <summary>
        /// Function in order to return the fact that the cell is dangerous or not.
        /// </summary>
        /// <returns>If the cell is dangerous or not</returns>
        public bool GetCellType() => IsDangerous;

        /// <summary>
        /// Redefine the equal operation between cells.
        /// </summary>
        /// <param name="other">The object to compare with the current cell.</param>
        /// <returns>true if the specified object is equal to the current cell; otherwise, false.</returns>
        public bool Equals(Cell? other)
        {
            if (other == null) return false;
            if (this.X == other.X && this.Y == other.Y) return true;
            return false;
        }


        public event PropertyChangedEventHandler? PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}