﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Game
{
    /// <summary>
    /// The Dice class represents a dice in the application.
    /// </summary>
    public class Dice
    {
        /// <summary>
        /// Lowest number on the dice.
        /// </summary>
        public int MinVal { get; private set; }

        /// <summary>
        /// Highest number on the dice.
        /// </summary>
        public int MaxVal { get; private set; }

        /// <summary>
        /// Value of the dice.
        /// </summary>
        private int _value;
        public int Value {
            get => _value;
            private set
            {
                if (value < MinVal || value > MaxVal)
                {
                    value = MinVal;
                }
                _value = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Dice"/> class.
        /// </summary>
        /// <param name="minval">The lowest number on the dice, on which the highest number is set</param>
        public Dice(int minval)
        {
            if (minval < 0) minval = 0;
            if (minval > 1) minval = 1;
            MinVal = minval;
            MaxVal = minval + 5;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Dice"/> class.
        /// </summary>
        public Dice()
        {
            MinVal = 0;
            MaxVal = 5;
        }

        /// <summary>
        /// Rolls the dice.
        /// </summary>
        public void Roll()
        {
            Value = new Random().Next(MinVal, MaxVal + 1);
        }

        /// <summary>
        /// Compare 2 dice values.
        /// </summary>
        /// <param name="dice2">Another dice</param>
        /// <returns>true if the dice2 is higher than the dice1; otherwise, false.</returns>
        public bool IsLower(Dice dice2)
        {
            return dice2.Value > this.Value;
        }
        
    }
}