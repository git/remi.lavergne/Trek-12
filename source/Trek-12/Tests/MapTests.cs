﻿using Models;
using Models.Game;

namespace Tests;

public class MapTests
{
    [Fact]
    public void Map_Initialization_SetsBackground()
    {
        string name = "test_name";
        string background = "test_background";

        var map = new Map(name, background);

        Assert.Equal(background, map.Background);
    }

    [Fact]
    public void Map_Initialization_InitializesBoards()
    {
        string name = "test_name";
        string background = "test_background";

        var map = new Map(name, background);

        Assert.Equal(49, map.Boards.Count);
        for (int i = 0; i < 36; i++)
        {
            Assert.Equal(new Cell(i % 7, i / 7), map.Boards[i]);
        }
    }

    [Fact]
    public void Map_Initialization_InitializesRopePathsAndZones()
    {
        string name = "test_name";
        string background = "test_background";

        var map = new Map(name, background);

        Assert.NotNull(map.RopePaths);
        Assert.NotNull(map.Zones);
        Assert.Empty(map.RopePaths);
        Assert.Empty(map.Zones);
    }

    [Fact]
    public void IsCellInZones_FoundTheCell()
    {
        var map = new Map("test_name", "test_background.png");

        var cell = new Cell(0, 0);
        var zone = new List<Cell> { cell };
        map.Zones.Add(zone);
        Assert.True(map.IsCellInZones(cell));

    }

    [Fact]
    public void IsCellInZones_DoNotFoundTheCell()
    {
        var map = new Map("test_name", "test_background.png");

        var cell = new Cell(0, 0);
        var othercell = new Cell(1, 1);
        var zone = new List<Cell> { cell };
        map.Zones.Add(zone);
        Assert.False(map.IsCellInZones(othercell));

    }

    [Fact]
    public void IsCellInRopePath_FoundTheCell()
    {
        var map = new Map("test_name", "test_background.png");

        var cell = new Cell(0, 0);
        var paths = new List<Cell> { cell };
        map.RopePaths.Add(paths);
        Assert.True(map.IsCellInRopePath(cell));

    }

    [Fact]
    public void IsCellInRopePath_DoNotFoundTheCell()
    {
        var map = new Map("test_name", "test_background.png");

        var cell = new Cell(0, 0);
        var othercell = new Cell(1, 1);
        var paths = new List<Cell> { cell };
        map.RopePaths.Add(paths);
        Assert.False(map.IsCellInRopePath(othercell));
    }
    
    [Fact]
    public void Clone_ReturnsNewMap()
    {
        var map = new Map("test_name", "test_background.png");

        var clone = map.Clone();

        Assert.NotEqual(map, clone);
        Assert.Equal(map.Name, clone.Name);
        Assert.Equal(map.Background, clone.Background);
    }
}
