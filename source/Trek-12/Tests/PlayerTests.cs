namespace Tests;
using Models.Game;

public class PlayerTests
{
    [Theory]
    [InlineData("Player", "DefaultProfilePicture")]
    [InlineData("John Doe", "N/A.png")]
    public void Constructor_WithPseudoAndProfilePicture_SetsPseudoAndProfilePictureCorrectly(string pseudo, string profilePicture)
    {
        var player = new Player(pseudo, profilePicture);
        Assert.Equal(pseudo, player.Pseudo);
        Assert.Equal(profilePicture, player.ProfilePicture);
        Assert.Null(player.LastPlayed);
    }

    [Theory]
    [InlineData("John Doe", "John Doe", true)]
    [InlineData("John Doe", "Jane Doe", false)]
    public void Equals_WithSameOrDifferentPseudo_ReturnsExpectedResult(string pseudo1, string pseudo2, bool expectedResult)
    {
        var player1 = new Player(pseudo1, "DefaultProfilePicture");
        var player2 = new Player(pseudo2, "DefaultProfilePicture");
        Assert.Equal(expectedResult, player1.Equals(player2));
    }

    [Theory]
    [InlineData("John Doe", "John Doe", true)]
    [InlineData("John Doe", "Jane Doe", false)]
    public void GetHashCode_ReturnsSameOrDifferentHashCodeForPseudo(string pseudo1, string pseudo2, bool expectedResult)
    {
        var player1 = new Player(pseudo1, "DefaultProfilePicture");
        var player2 = new Player(pseudo2, "DefaultProfilePicture");
        Assert.Equal(expectedResult, player1.GetHashCode() == player2.GetHashCode());
    }

    [Fact]
    public void Equals_WithNull_ReturnsFalse()
    {
        Player player = new Player(null, null);
        Assert.False(player.Equals(null));
    }

    [Fact]
    public void Equals_WithDifferentType_ReturnsFalse()
    {
        Player player = new Player("test_pseudo", "DefaultProfilePicture");
        Assert.False(player.Equals(new Cell(0, 0)));
    }
    
    [Fact]
    public void UpdateLastPlayed_UpdatesLastPlayedDate()
    {
        var player = new Player("John Doe", "DefaultProfilePicture");
        
        player.UpdateLastPlayed();
        
        Assert.NotNull(player.LastPlayed);
        Assert.Equal(DateTime.Now.ToString("dd/MM/yyyy"), player.LastPlayed);
    }
}