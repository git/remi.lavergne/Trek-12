﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Game;
using Models.Interfaces;
using Moq;

namespace Tests
{
    public class Base64ConverterTests
    {
        /*
         public interface IImageConverter
           {
               /// <summary>
               /// Converter to any type that can be converted to a string (ex: Base64)
               /// </summary>
               /// <param name="imagePath"></param>
               /// <returns></returns>
               string ConvertImage(string imagePath);
           
               /// <summary>
               /// Retrieve an image from a string encoded
               /// </summary>
               /// <param name="imageString"></param>
               /// <returns></returns>
               ImageSource RetrieveImage(string imageString);
           }
         */

        /*
         public class Base64Converter : IImageConverter
           {
               /// <summary>
               /// Converts an image to a base64 string
               /// </summary>
               /// <param name="imagePath">The path to access the image</param>
               /// <returns>The base64 string representation of the image</returns>
               /// <exception cref="FileNotFoundException">Native .NET exception</exception>
               public string ConvertImage(string imagePath)
               {
                   if (!File.Exists(imagePath))
                   {
                       // native .NET exception
                       throw new FileNotFoundException("Image file not found", imagePath);
                   }
           
                   byte[] imageBytes = File.ReadAllBytes(imagePath);
                   return Convert.ToBase64String(imageBytes);
               }
           
               /// <summary>
               /// Retrieve an image from a string encoded in base64
               /// </summary>
               /// <param name="imageString"></param>
               /// <returns></returns>
               public ImageSource RetrieveImage(string imageString)
               {
                   byte[] imageBytes = Convert.FromBase64String(imageString);
                   return ImageSource.FromStream(() => new MemoryStream(imageBytes));
               }
           }
         */

        // Create a mock
        private Mock<IImageConverter> _imageConverterMock;

        public Base64ConverterTests()
        {
            _imageConverterMock = new Mock<IImageConverter>();
        }

        [Fact]
        public void ConvertImage_WithNonExistentPath_ThrowsFileNotFoundException()
        {
            var base64Converter = new Base64Converter();
            Assert.Throws<FileNotFoundException>(() => base64Converter.ConvertImage("nonexistent.jpg"));
        }

        [Fact]
        public void RetrieveImage_WithInvalidBase64_ThrowsFormatException()
        {
            var base64Converter = new Base64Converter();
            Assert.Throws<FormatException>(() => base64Converter.RetrieveImage("////invalidBase64//!!///"));
        }
    }
}
