﻿namespace Tests;
using Models.Game;

public class CellTests
{
    [Fact]
    public void CellConstructor_SetsCorrectValues()
    {
        Cell cell = new Cell(1, 2, true);

        Assert.Equal(1, cell.X);
        Assert.Equal(2, cell.Y);
        Assert.True(cell.GetCellType());
    }

    [Fact]
    public void CellConstructor_SetsDefaultValues()
    {
        Cell cell = new Cell(1, 2);

        Assert.Equal(1, cell.X);
        Assert.Equal(2, cell.Y);
        Assert.False(cell.GetCellType());
    }

    [Fact]
    public void ValueSetter_ThrowsException_WhenValueIsNegative()
    {
        Cell cell = new Cell(1, 2);

        Assert.Throws<Exception>(() => cell.Value = -1);
    }

    [Fact]
    public void ValueSetter_SetsValue_WhenValueIsPositive()
    {
        Cell cell = new Cell(1, 2);

        cell.Value = 5;

        Assert.Equal(5, cell.Value);
    }

    [Fact]
    public void ValueSetter_SetsValue_WhenValueIsNull()
    {
        Cell cell = new Cell(1, 2);
        Assert.Null(cell.Value);
    }
}