﻿using System.Collections.ObjectModel;
using Moq;
using Models.Game;
using Models.Interfaces;
using System.Reflection;
using static System.Type;
using Xunit.Abstractions;

namespace Tests;

public class GameTests
{
    private readonly Mock<IPersistence> _mockPersistence; // Mocking (faking) the persistence layer to allow for testing without a persistent connection
    private readonly Game _game;

    public GameTests()
    {
        _mockPersistence = new Mock<IPersistence>();
        _game = new Game(_mockPersistence.Object);
    }

    private void SetDiceValues(Game game, int value1, int value2)
    {
        var dice1Field = typeof(Game).GetProperty("Dice1", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        var dice2Field = typeof(Game).GetProperty("Dice2", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

        if (dice1Field != null && dice2Field != null)
        {
            var dice1 = (Dice)dice1Field?.GetValue(game)!;
            var dice2 = (Dice)dice2Field?.GetValue(game)!;

            var valueField = typeof(Dice).GetField("_value", BindingFlags.Instance | BindingFlags.NonPublic);
            valueField?.SetValue(dice1, value1);
            valueField?.SetValue(dice2, value2);
        }
    }

    [Fact]
    public void DefaultConstructor_ShouldInitializeLists()
    {
        Assert.NotNull(_game.Players);
        Assert.NotNull(_game.Games);
        Assert.NotNull(_game.Maps);
        Assert.NotNull(_game.BestScores);
        Assert.NotNull(_game.GameRules);
        Assert.False(_game.IsRunning);
    }

    [Fact]
    public void AddPlayer_ShouldAddPlayerToList()
    {
        var player = new Player("test_player", "DefaultProfilePicture");

        _game.AddPlayer(player);

        Assert.Contains(player, _game.Players);
    }

    [Fact]
    public void AddGame_ShouldAddGameToList()
    {
        var game = new Game(_mockPersistence.Object);

        _game.AddGame(game);

        Assert.Contains(game, _game.Games);
    }

    [Fact]
    public void AddMap_ShouldAddMapToList()
    {
        var map = new Map("test_name", "test_background.png");

        _game.AddMap(map);

        Assert.Contains(map, _game.Maps);
    }

    [Fact]
    public void AddBestScore_ShouldAddBestScoreToList()
    {
        var bestScore = new BestScore("test", new Player("John", "Picture.png"), 3, 127);

        _game.BestScores.Add(bestScore);

        Assert.Contains(bestScore, _game.BestScores);
    }

    [Fact]
    public void LoadData_ShouldLoadDataFromPersistence()
    {
        var myGame = new Game(_mockPersistence.Object);
        var myPlayer = new Player("test", "DefaultProfilePicture");
        var myMap = new Map("test_name", "test_background.png");
        
        var players = new ObservableCollection<Player> { myPlayer };
        var games = new ObservableCollection<Game> { myGame };
        var maps = new ObservableCollection<Map> { myMap };
        var bestScores = new ObservableCollection<BestScore> { new BestScore(myMap.Name, myPlayer, 1, 45) };

        _mockPersistence.Setup(p => p.LoadData()).Returns((players, games, maps, bestScores));

        _game.LoadData();

        Assert.Equal(players, _game.Players);
        Assert.Equal(games, _game.Games);
        Assert.Equal(maps, _game.Maps);
        Assert.Equal(bestScores, _game.BestScores);
    }

    [Fact]
    public void SaveData_ShouldCallPersistenceSaveData()
    {
        _game.SaveData();

        _mockPersistence.Verify(p => p.SaveData(_game.Players, _game.Games, _game.Maps, _game.BestScores), Times.Once); // Times.Once is to verify if the method is called exactly one time
    }

    [Fact]
    public void InitializeGame_ShouldInitializeGameAndNotTriggerEventWhenNotStarted()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");
        bool eventTriggered = false;

        _game.GameStarted += (sender, args) =>
        {
            eventTriggered = true;
        };

        _game.InitializeGame(map, player, false);
        Assert.False(eventTriggered);
        Assert.False(_game.IsRunning);
        Assert.Equal(map, _game.UsedMap);
        Assert.Equal(player, _game.CurrentPlayer);
    }

    [Theory]
    [InlineData(Operation.ADDITION, 3, 4, 7)]
    [InlineData(Operation.SUBTRACTION, 6, 4, 2)]
    [InlineData(Operation.MULTIPLICATION, 2, 3, 6)]
    [InlineData(Operation.LOWER, 1, 4, 1)]
    [InlineData(Operation.HIGHER, 2, 5, 5)]
    public void HandlePlayerOperation_ShouldPerformCorrectOperationAndTriggerEvent(Operation operation, int value1, int value2, int expectedResult)
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");
        _game.InitializeGame(map, player, false);

        bool eventTriggered = false;
        int actualResult = 0;

        _game.OperationChosen += (sender, args) =>
        {
            eventTriggered = true;
            actualResult = args.Result;
        };

        SetDiceValues(_game, value1, value2);


        _game.HandlePlayerOperation(operation);


        Assert.True(eventTriggered);
        Assert.Equal(expectedResult, actualResult);
    }

    [Fact]
    public void Game_Initialization_SetsMap()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        _game.InitializeGame(map, player, false);

        Assert.Equal(map, _game.UsedMap);
    }

    [Fact]
    public void Game_Initialization_SetsPlayer()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        _game.InitializeGame(map, player, false);

        Assert.Equal(player, _game.CurrentPlayer);
    }

    [Fact]
    public void Game_Initialization_SetsDice()
    {
        Player player = new Player("test_player", "DefaultProfilePicture");
        Map map = new Map("test_name", "test_background.png");

        _game.InitializeGame(map, player, false);

        Assert.NotNull(_game.Dice1);
        Assert.NotNull(_game.Dice2);
    }

    [Fact]
    public void Game_Initialization_SetsGameRules()
    {
        var game = new Game(_mockPersistence.Object);

        Assert.NotNull(game.GameRules);
    }

    [Fact]
    public void MarkOperationAsChecked_Check_Well()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        _game.InitializeGame(map, player, false);

        // Use of reflection to call private method
        var methodInfo = typeof(Game).GetMethod("MarkOperationAsChecked", BindingFlags.NonPublic | BindingFlags.Instance);
        Assert.NotNull(methodInfo);

        var operation = Operation.ADDITION;


        methodInfo.Invoke(_game, new object[] { operation });
        
        int operationIndex = (int)operation;
        int operationsPerType = 4;
        bool isChecked = false;

        for (int i = operationIndex * operationsPerType; i < (operationIndex + 1) * operationsPerType; i++)
        {
            if (_game.UsedMap.OperationGrid[i].IsChecked)
            {
                isChecked = true;
                break;
            }
        }

        Assert.True(isChecked);
    }

    [Fact]
    public void IsPlaceOperationCorrect()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        _game.InitializeGame(map, player, false);
        Assert.NotNull(_game.GameRules);
        _game.UsedMap.Boards[0].Value = 1;
        _game.UsedMap.Boards[1].Value = 2;
        _game.UsedMap.Boards[0].Valid = true;
        _game.UsedMap.Boards[1].Valid = true;
        _game.UsedMap.Boards[2].Valid = true;

        var methodInfo = typeof(Game).GetMethod("PlaceResult", BindingFlags.NonPublic | BindingFlags.Instance);
        Assert.NotNull(methodInfo);

        _game.PlayerCell = new Cell(2, 0);
        _game.PlayerCell.Value = 3;
        methodInfo.Invoke(_game, new object[] { _game.PlayerCell, 3 });
        //_game.UsedMap.Boards[2].Value = _game.PlayerCell.Value;

        Assert.Equal(3, _game.UsedMap.Boards[2].Value);
    }

    [Fact]
    public void IsHandlePlayerChoice_Handling()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        _game.InitializeGame(map, player, false);

        var cell = new Cell(1, 0);
        cell.Valid = true;
        _game.UsedMap.Boards[0].Valid = true;
        _game.UsedMap.Boards[0].Value = 1;
        _game.UsedMap.Boards[1].Valid = true;
        bool result = _game.HandlePlayerChoice(cell, 1);
        Assert.True(result);
    }

    [Fact]
    public void IsHandlePlayerChoice_InvalidCell()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        _game.InitializeGame(map, player, false);

        var cell = new Cell(0, 7);
        cell.Value = 1;
        bool result = _game.HandlePlayerChoice(cell, 1);
        Assert.False(result);
    }

    [Fact]
    public void IsHandlePlayerChoice_InvalidPlace()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        _game.InitializeGame(map, player, false);

        var cell = new Cell(0, 0);
        cell.Value = 1;
        var othercell = new Cell(3, 3);
        bool result = _game.HandlePlayerChoice(othercell, 1);
        Assert.False(result);
    }

    [Fact]
    public void RemovePlayerTest_ShouldRemovePlayer()
    { 
        Game game = new Game();
        Player player = new Player("test", "DefaultProfilePicture");
        game.AddPlayer(player);
        Assert.True(game.RemovePlayer("test"));
    }

    [Fact]
    public void RemovePlayerTest_ShouldNotRemovePlayer()
    {
        Game game = new Game();
        Player player = new Player("test", "DefaultProfilePicture");
        game.AddPlayer(player);
        Assert.False(game.RemovePlayer("otherName"));
    }

    [Fact]
    public void PutPenalty_ShouldPutPenalty()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");
        _game.InitializeGame(map, player, false);
        _game.UsedMap.Boards[1].Valid = true;
        _game.UsedMap.Boards[2].Valid = true;
        _game.UsedMap.Boards[1].Value = 5;

        var methodInfo = typeof(Game).GetMethod("PlaceResult", BindingFlags.NonPublic | BindingFlags.Instance);
        Assert.NotNull(methodInfo);

        _game.PlayerCell = new Cell(2, 0);
        _game.PlayerCell.Value = 14;
        methodInfo.Invoke(_game, new object[] { _game.PlayerCell, 14 });

        Assert.True(_game.UsedMap.Boards[2].Penalty);
    }

    [Fact]
    public void PutPenalty_ShouldPutPenalty_ForDangerous()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");
        _game.InitializeGame(map, player, false);

        _game.UsedMap.Boards[1].Valid = true;
        _game.UsedMap.Boards[2].Valid = true;

        _game.UsedMap.Boards[1].Value = 5;
        _game.UsedMap.Boards[2].IsDangerous = true;

        var methodInfo = typeof(Game).GetMethod("PlaceResult", BindingFlags.NonPublic | BindingFlags.Instance);
        Assert.NotNull(methodInfo);

        _game.PlayerCell = new Cell(2, 0);
        _game.PlayerCell.Value = 7;
        methodInfo.Invoke(_game, new object[] { _game.PlayerCell, 7 });

        Assert.True(_game.UsedMap.Boards[2].Penalty);
    }

    [Fact]
    public void PutPenaltyForLostCells_ReallyPutPenalty_ForZones()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");
        _game.InitializeGame(map, player, false);

        _game.UsedMap.Boards[0].Valid = true;
        _game.UsedMap.Boards[3].Valid = true;
        _game.UsedMap.Boards[2].Valid = true;
        _game.UsedMap.Boards[1].Valid = true;

        _game.UsedMap.Boards[1].Value = 5;
        _game.UsedMap.Boards[2].Value = 5;
        _game.UsedMap.Boards[3].Value = 5;
        _game.UsedMap.Boards[0].Value = 0;

        foreach (var cells in _game.UsedMap.Boards.ToList())
        {
            _game.GameRules.IsZoneValidAndAddToZones(cells, _game.UsedMap);
        }

        _game.PutPenaltyForLostCells(_game.UsedMap.Boards);

        Assert.True(_game.UsedMap.Boards[0].Penalty);

    }

    [Fact]
    public void PutPenaltyForLostCells_ReallyPutPenalty_ForRopePathes()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");
        _game.InitializeGame(map, player, false);

        _game.UsedMap.Boards[1].Value = 1;
        _game.UsedMap.Boards[2].Value = 2;
        _game.UsedMap.Boards[3].Value = 5;
        _game.UsedMap.Boards[0].Value = 0;

        var methodInfo = typeof(Game).GetMethod("AddToRopePath", BindingFlags.NonPublic | BindingFlags.Instance);
        Assert.NotNull(methodInfo);

        foreach (var cells in _game.UsedMap.Boards.ToList())
        {
            methodInfo.Invoke(_game, new object[] { cells });
        }

        _game.PutPenaltyForLostCells(_game.UsedMap.Boards);

        Assert.True(_game.UsedMap.Boards[3].Penalty);

    }

    [Fact]
    public void CalculusOfPenalty_ReallyCalculusPenalty_ForZonesAndDangerousCellsAndOverTwelve()
    {
        var player = new Player("test_player", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");
        _game.InitializeGame(map, player, false);

        _game.UsedMap.Boards[7].Valid = true;
        _game.UsedMap.Boards[8].Valid = true;
        _game.UsedMap.Boards[9].Valid = true;
        _game.UsedMap.Boards[10].Valid = true;
        _game.UsedMap.Boards[11].Valid = true;
        _game.UsedMap.Boards[12].Valid = true;

        _game.UsedMap.Boards[10].Value = 2; // 1,3 // penalty
        _game.UsedMap.Boards[7].Value = 5; // 1,0
        _game.UsedMap.Boards[8].Value = 5; // 1,1
        _game.UsedMap.Boards[9].Value = 5; // 1,2

        var place = typeof(Game).GetMethod("PlaceResult", BindingFlags.NonPublic | BindingFlags.Instance);
        Assert.NotNull(place);

        _game.PlayerCell = new Cell(4, 1);
        _game.PlayerCell.Value = 7;
        _game.PlayerCell.Valid = true;
        _game.PlayerCell.IsDangerous = true;
        place.Invoke(_game, new object[] { _game.PlayerCell, 7 }); //One penalty

        _game.PlayerCell = new Cell(5, 1);
        _game.PlayerCell.Value = 14;
        _game.PlayerCell.Valid = true;
        place.Invoke(_game, new object[] { _game.PlayerCell, 14 });


        foreach (var cells in _game.UsedMap.Boards.ToList())
        {
            _game.GameRules.IsZoneValidAndAddToZones(cells, _game.UsedMap);
        }

        _game.PutPenaltyForLostCells(_game.UsedMap.Boards);

        Assert.True(_game.UsedMap.Boards[11].Penalty);
        Assert.Equal(9, _game.CalculusOfPenalty(_game.UsedMap.Boards));

    }

    [Fact]
    public void CanIModifyAPlayer()
    {
        Game game = new Game();
        Player player = new Player("test", "DefaultProfilePicture");
        game.AddPlayer(player);
        game.ModifyPlayer("test", "newName");
        Assert.Equal("newName", game.Players[0].Pseudo);
    }

    [Fact]
    public void CanIModifyANonExistentPlayer()
    {
        Game game = new Game();
        var res = game.ModifyPlayer("nope", "newName");
        Assert.False(res);
    }

    [Fact]
    public void CanIRollDice()
    {
        _game.InitializeGame(new Map("test", "test.png"), new Player("test", "test.png"), false);
        _game.RollAllDice();
        Assert.NotNull(_game.Dice1);
        Assert.NotNull(_game.Dice2);
        Assert.True(_game.Dice1.Value >= 0 && _game.Dice1.Value <= 5 );
        Assert.True(_game.Dice2.Value >= 1 && _game.Dice2.Value <= 6 );
    }

    [Fact]
    public void CanIStartGame()
    {
        _game.InitializeGame(new Map("test", "test.png"), new Player("test", "test.png"), false);

        var start = typeof(Game).GetMethod("StartGame", BindingFlags.NonPublic | BindingFlags.Instance);
        Assert.NotNull(start);
        
        start.Invoke(_game, null);

        Assert.True(_game.IsRunning);
    }

    [Fact]
    public void CanIEndGame()
    {
        _game.InitializeGame(new Map("test", "test.png"), new Player("test", "test.png"), false);

        var start = typeof(Game).GetMethod("StartGame", BindingFlags.NonPublic | BindingFlags.Instance);
        Assert.NotNull(start);

        start.Invoke(_game, null);
        var end = typeof(Game).GetMethod("EndGame", BindingFlags.NonPublic | BindingFlags.Instance);
        Assert.NotNull(end);

        end.Invoke(_game, new object[] { 14 } );

        Assert.False(_game.IsRunning);
    }

    [Fact]
    public void CalculusOfPointsWorksWellOrNot()
    {
        _game.InitializeGame(new Map("test", "test.png"), new Player("test", "test.png"), false);

        _game.UsedMap.Boards[7].Valid = true;
        _game.UsedMap.Boards[8].Valid = true;
        _game.UsedMap.Boards[9].Valid = true;
        _game.UsedMap.Boards[10].Valid = true;
        _game.UsedMap.Boards[11].Valid = true;
        _game.UsedMap.Boards[12].Valid = true;

        _game.UsedMap.Boards[10].Value = 2; // penalty (- 3)
        _game.UsedMap.Boards[7].Value = 5; //5 + 2 = 7
        _game.UsedMap.Boards[8].Value = 5;
        _game.UsedMap.Boards[9].Value = 5;

        var place = typeof(Game).GetMethod("PlaceResult", BindingFlags.NonPublic | BindingFlags.Instance);
        Assert.NotNull(place);

        _game.PlayerCell = new Cell(4, 1);
        _game.PlayerCell.Value = 7;
        _game.PlayerCell.Valid = true;
        _game.PlayerCell.IsDangerous = true;
        place.Invoke(_game, new object[] { _game.PlayerCell, 7 }); //One penalty



        foreach (var cells in _game.UsedMap.Boards.ToList())
        {
            _game.GameRules.IsZoneValidAndAddToZones(cells, _game.UsedMap);
        }

        _game.PutPenaltyForLostCells(_game.UsedMap.Boards);

        Assert.True(_game.UsedMap.Boards[11].Penalty);

        Assert.Equal(1, _game.FinalCalculusOfPoints());
    }

    [Fact]
    public void CalculusOfPointsWorksWellOrNotForRopePathes()
    {
        _game.InitializeGame(new Map("test", "test.png"), new Player("test", "test.png"), false);

        var methodInfo = typeof(Game).GetMethod("AddToRopePath", BindingFlags.NonPublic | BindingFlags.Instance);
        Assert.NotNull(methodInfo);
        _game.Turn = 2;
        _game.UsedMap.Boards[7].Valid = true;
        _game.UsedMap.Boards[8].Valid = true;
        _game.UsedMap.Boards[9].Valid = true;
        _game.UsedMap.Boards[10].Valid = true;

        _game.UsedMap.Boards[7].Value = 5; //7 + 2 = 9
        methodInfo.Invoke(_game, new object[] { _game.UsedMap.Boards[7] });
        _game.UsedMap.Boards[8].Value = 6;
        methodInfo.Invoke(_game, new object[] { _game.UsedMap.Boards[8] });
        _game.UsedMap.Boards[9].Value = 7;
        methodInfo.Invoke(_game, new object[] { _game.UsedMap.Boards[9] });
        _game.UsedMap.Boards[10].Value = 2; // penalty (- 3)
        methodInfo.Invoke(_game, new object[] { _game.UsedMap.Boards[10] });


        _game.PutPenaltyForLostCells(_game.UsedMap.Boards);


        Assert.True(_game.UsedMap.Boards[10].Penalty);

        Assert.Equal(6, _game.FinalCalculusOfPoints());
    }
    
    [Fact]
    public void AddBestScore_AddsNewBestScoreToList()
    {
        var game = new Game(_mockPersistence.Object);
        var player = new Player("John Doe", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        game.InitializeGame(map, player, false);

        game.AddBestScore(100);

        Assert.Single(game.BestScores);
        Assert.Equal(100, game.BestScores[0].Score);
        Assert.Equal(player, game.BestScores[0].ThePlayer);
        Assert.Equal(map.Name, game.BestScores[0].MapName);
    }

    [Fact]
    public void AddBestScore_UpdatesExistingBestScoreAndIncrementsGamesPlayed()
    {
        var game = new Game(_mockPersistence.Object);
        var player = new Player("John Doe", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        game.InitializeGame(map, player, false);

        game.AddBestScore(100);
        game.AddBestScore(200);

        Assert.Single(game.BestScores);
        Assert.Equal(300, game.BestScores[0].Score);
        Assert.Equal(2, game.BestScores[0].GamesPlayed);
        Assert.Equal(player, game.BestScores[0].ThePlayer);
        Assert.Equal(map.Name, game.BestScores[0].MapName);
    }

    [Fact]
    public void AddBestScore_SortsBestScoresCorrectly()
    {
        var game = new Game(_mockPersistence.Object);
        var player1 = new Player("John Doe", "DefaultProfilePicture");
        var player2 = new Player("John Does", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        game.InitializeGame(map, player1, false);

        game.AddBestScore(100);
        game.InitializeGame(map, player2, false);
        game.AddBestScore(200);

        Assert.Equal(2, game.BestScores.Count);
        Assert.Equal(200, game.BestScores[0].Score);
        Assert.Equal(player2, game.BestScores[0].ThePlayer);
        Assert.Equal(100, game.BestScores[1].Score);
        Assert.Equal(player1, game.BestScores[1].ThePlayer);
    }
    
    [Fact]
    public void CheckAndRemoveBestScoresDependencies_RemovesDependenciesCorrectly()
    {
        var game = new Game(_mockPersistence.Object);
        var player = new Player("John Doe", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        game.InitializeGame(map, player, false);
        game.AddBestScore(100);

        game.CheckAndRemoveBestScoresDependencies(player.Pseudo);

        Assert.DoesNotContain(game.BestScores, bs => bs.ThePlayer.Pseudo == player.Pseudo);
    }

    [Fact]
    public void CheckAndChangeBestScoresDependencies_ChangesDependenciesCorrectly()
    {
        var game = new Game(_mockPersistence.Object);
        var player = new Player("John Doe", "DefaultProfilePicture");
        var map = new Map("test_name", "test_background.png");

        game.InitializeGame(map, player, false);
        game.AddBestScore(100);

        game.CheckAndChangeBestScoresDependencies(player.Pseudo, "John Does");

        Assert.All(game.BestScores, bs => Assert.NotEqual("John Doe", bs.ThePlayer.Pseudo));
        Assert.Contains(game.BestScores, bs => bs.ThePlayer.Pseudo == "John Does");
    }

}
