﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models.Game;

namespace Tests;

public class DiceTests
{
    [Fact]
    public void Constructor_WithNegativeNbMin_SetsNbMinToZero()
    {
        Dice dice = new Dice(-1);
        Assert.Equal(0, dice.MinVal);
    }

    [Fact]
    public void Constructor_WithValueMoreThan5_SetsValueTo0()
    {
        Dice dice = new Dice(6);
        Assert.Equal(0, dice.Value);
    }

    [Fact]
    public void Constructor_WithNbMinGreaterThanOne_SetsNbMinToOne()
    {
        Dice dice = new Dice(2);
        Assert.Equal(1, dice.MinVal);
    }

    [Fact]
    public void Constructor_WithValidNbMin_SetsNbMinAndNbMaxCorrectly()
    {
        Dice dice = new Dice(1);
        Assert.Equal(1, dice.MinVal);
        Assert.Equal(6, dice.MaxVal);
    }

    [Fact]
    public void DefaultConstructor_SetsNbMinToZeroAndNbMaxToFive()
    {
        Dice dice = new Dice();
        Assert.Equal(0, dice.MinVal);
        Assert.Equal(5, dice.MaxVal);
    }

    [Fact]
    public void Roll_SetsValueBetweenNbMinAndNbMax()
    {
        Dice dice = new Dice();
        dice.Roll();
        Assert.True(dice.Value >= dice.MinVal && dice.Value <= dice.MaxVal);
    }
}
