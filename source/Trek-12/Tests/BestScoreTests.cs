﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Models.Game;

namespace Tests
{
    public class BestScoreTests
    {
        [Fact]
        public void Constructor_WithNegativeInputs_SetsPropertiesToZero()
        {
            var myMap = new Map("Dunai", "Dunai.png");
            var myPlayer = new Player("John", "pp.png");

            var bestScore = new BestScore(myMap.Name, myPlayer, -5, -10);
            
            Assert.Equal(0, bestScore.GamesPlayed);
            Assert.Equal(0, bestScore.Score);
        }

        [Fact]
        public void Constructor_WithPositiveInputs_SetsPropertiesCorrectly()
        {
            var myMap = new Map("Dunai", "Dunai.png");
            var myPlayer = new Player("John", "pp.png");

            var bestScore = new BestScore(myMap.Name, myPlayer, 5, 10);
            
            Assert.Equal(5, bestScore.GamesPlayed);
            Assert.Equal(10, bestScore.Score);
        }

        [Fact]
        public void IncrGamesPlayed_IncrementsGamesPlayed()
        {
            var myMap = new Map("Dunai", "Dunai.png");
            var myPlayer = new Player("John", "pp.png");

            var bestScore = new BestScore(myMap.Name, myPlayer, 0, 0);
            bestScore.IncrGamesPlayed();
            
            Assert.Equal(1, bestScore.GamesPlayed);
        }

        [Fact]
        public void ScoreSetter_WithLowerValue_DoesNotChangeScore()
        {
            var myMap = new Map("Dunai", "Dunai.png");
            var myPlayer = new Player("John", "pp.png");

            var bestScore = new BestScore(myMap.Name, myPlayer, 0, 10);
            bestScore.UpdateScore(5);
            
            Assert.Equal(15, bestScore.Score);
        }

        [Fact]
        public void ScoreSetter_WithHigherValue_ChangesScore()
        {
            var myMap = new Map("Dunai", "Dunai.png");
            var myPlayer = new Player("John", "pp.png");

            var bestScore = new BestScore(myMap.Name, myPlayer, 0, 5);
            bestScore.UpdateScore(10);
            
            Assert.Equal(15, bestScore.Score);
        }
        
        [Fact]
        public void IdGetter_ReturnsCorrectId()
        {
            var myMap = new Map("Dunai", "Dunai.png");
            var myPlayer = new Player("John", "pp.png");

            var bestScore = new BestScore(myMap.Name, myPlayer, 5, 10);
            bestScore.Id = 1;

            Assert.Equal(1, bestScore.Id);
        }

        [Fact]
        public void Equals_WithEqualBestScores_ReturnsTrue()
        {
            var myMap = new Map("Dunai", "Dunai.png");
            var myPlayer = new Player("John", "pp.png");

            var bestScore1 = new BestScore(myMap.Name, myPlayer, 5, 10);
            var bestScore2 = new BestScore(myMap.Name, myPlayer, 5, 10);

            Assert.True(bestScore1.Equals(bestScore2));
        }

        [Fact]
        public void Equals_WithDifferentBestScores_ReturnsFalse()
        {
            var myMap1 = new Map("Dunai", "Dunai.png");
            var myMap2 = new Map("Amazon", "Amazon.png");
            var myPlayer = new Player("John", "pp.png");

            var bestScore1 = new BestScore(myMap1.Name, myPlayer, 5, 10);
            var bestScore2 = new BestScore(myMap2.Name, myPlayer, 5, 10);

            Assert.False(bestScore1.Equals(bestScore2));
        }

        [Fact]
        public void GetHashCode_WithEqualBestScores_ReturnsSameHashCode()
        {
            var myMap = new Map("Dunai", "Dunai.png");
            var myPlayer = new Player("John", "pp.png");

            var bestScore1 = new BestScore(myMap.Name, myPlayer, 5, 10);
            var bestScore2 = new BestScore(myMap.Name, myPlayer, 5, 10);

            Assert.Equal(bestScore1.GetHashCode(), bestScore2.GetHashCode());
        }

        [Fact]
        public void GetHashCode_WithDifferentBestScores_ReturnsDifferentHashCodes()
        {
            var myMap1 = new Map("Dunai", "Dunai.png");
            var myMap2 = new Map("Amazon", "Amazon.png");
            var myPlayer = new Player("John", "pp.png");

            var bestScore1 = new BestScore(myMap1.Name, myPlayer, 5, 10);
            var bestScore2 = new BestScore(myMap2.Name, myPlayer, 5, 10);

            Assert.NotEqual(bestScore1.GetHashCode(), bestScore2.GetHashCode());
        }
    }
}
