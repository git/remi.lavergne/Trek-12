﻿using Models.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    public class OperationCellTests
    {
        [Fact]
        public void Constructor_WithValidCoordinates_SetsCoordinatesCorrectly()
        {
            OperationCell operationCell = new OperationCell(1, 2);
            Assert.Equal(1, operationCell.X);
            Assert.Equal(2, operationCell.Y);
        }

        [Fact]
        public void Constructor_WithValidCoordinates_SetsIsCheckedToFalse()
        {
            OperationCell operationCell = new OperationCell(1, 2);
            Assert.False(operationCell.IsChecked);
        }
    }
}
