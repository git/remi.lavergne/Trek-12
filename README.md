![Static Badge](https://img.shields.io/badge/Trek12-Disponible_au_public-yellow)

<details>
  <summary>Sonarcube Ratings</summary>
  
  [![Maintainability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Trek-12&metric=sqale_rating&token=7584294f89189be7a1ed771405ce4114938dca12)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Trek-12)
[![Reliability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Trek-12&metric=reliability_rating&token=7584294f89189be7a1ed771405ce4114938dca12)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Trek-12)
[![Security Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Trek-12&metric=security_rating&token=7584294f89189be7a1ed771405ce4114938dca12)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Trek-12)
</details>

[![Build Status](https://codefirst.iut.uca.fr/api/badges/remi.lavergne/Trek-12/status.svg)](https://codefirst.iut.uca.fr/remi.lavergne/Trek-12)
[![Quality Gate Status](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Trek-12&metric=alert_status&token=7584294f89189be7a1ed771405ce4114938dca12)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Trek-12)
[![Bugs](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Trek-12&metric=bugs&token=7584294f89189be7a1ed771405ce4114938dca12)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Trek-12)
[![Coverage](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Trek-12&metric=coverage&token=7584294f89189be7a1ed771405ce4114938dca12)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Trek-12)
[![Vulnerabilities](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Trek-12&metric=vulnerabilities&token=7584294f89189be7a1ed771405ce4114938dca12)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Trek-12)



<img src="trek12.png" alt="Trek12" width="40%" style="align: center; display: block; margin: 20px auto;">

<br>

Auteurs:
- [Lucas DUFLOT](https://codefirst.iut.uca.fr/git/lucas.duflot)
- [Rémi LAVERGNE](https://codefirst.iut.uca.fr/git/remi.lavergne)
- [Rémi NEVEU](https://codefirst.iut.uca.fr/git/remi.neveu)

<br>

> ### **📼 Vidéo de présentation sur [Opencast](https://opencast.dsi.uca.fr/paella/ui/watch.html?id=b08a0497-0fc3-420f-a98d-5f5a2b226252).**
> ### **📕 Toute la conception et le fonctionnement est présent sur le [Wiki](https://codefirst.iut.uca.fr/git/remi.lavergne/Trek-12/wiki) !**
> ### **📄 Documentation du code sur [CodeDoc](https://codefirst.iut.uca.fr/documentation/remi.lavergne/Trek-12/doxygen/index.html).**

<br>

*Persistance locale en XML ou JSON disponible. La persistance en SQLite est créée mais nécessite encore du développement pour fonctionner.*<br/>
*Pour changer de mode de persistance, se rendre dans le fichier App.xaml.cs*

<br><br>

## 📦 Générer un exécutable

Pour générer un exécutable, il suffit de lancer la commande suivante dans le terminal :

```bash
# Pour un exécutable MSIX (Windows Store):
dotnet publish -f net8.0-windows10.0.19041.0 -p:WindowsPackageType=MSIX

# Ou pour un exécutable "classique":
dotnet publish -c Release -r win-x64 --self-contained
```

<br>

***Avant de fork le projet ou autre, merci de lire attentivement la [LICENCE](LICENSE.md)***